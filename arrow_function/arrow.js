// function expression
// const sapa = function(nama) {
//     return `Halo ${nama}`;
// }
// console.log(sapa("Agus"));

// arrow function
// const sapaNama = (nama) => { return `Halo ${nama}`; }
// console.log(sapaNama("Mahardika"));

// 2 parameter
// const sapa = (waktu, nama) => {
//     return `Selamat ${waktu}, ${nama}!`;
// }
// console.log(sapa("siang", "Mahardika"));

// 1 parameter bisa tidak diisi kurung
// hanya 1 baris dan hanya return bisa tidak pakai kurung kurawal dan kata return dihilangkan
// const halo = nama => `Halo ${nama}`;
// console.log(halo("Made"));

// tanpa parameter harus tulis kurung
// const mulai = () => `Hello world!`;
// console.log(mulai());

// bisa digunakan di fungsi map
let mahasiswa = ["anto", "budi", "mahardika"];
// function expression
let jumlahKarakter = mahasiswa.map(function(nama) {
    return nama.length;
});
console.log(jumlahKarakter);

// menggunakan arrow function
let jumlahKarakterArrow = mahasiswa.map(nama => nama.length);
console.log(jumlahKarakterArrow);

// mengembalikan object
// let jumlah = mahasiswa.map(nama => ({nama: nama, karakter: nama.length}));
// atau jika properti object sama dengan nama variabel
let jumlah = mahasiswa.map(nama => ({nama, karater: nama.length}));
console.table(jumlah);