// constructor function
// const Mahasiswa = function() {
//     this.nama = 'Mahardika';
//     this.umur = 18;
//     this.sayHello = function() {
//         console.log(`Halo, nama saya ${this.nama}, ${this.umur} tahun`);
//     }
// }
// const mahardika = new Mahasiswa();
// console.log(mahardika.sayHello());

// bentuk arrow function
// constructor tidak bisa diubah ke arraow function
// const Mahasiswa = function() {
//     this.nama = "Mahardika";
//     this.umur = 18;
//     this.sayHello = () => {
//         console.log(`Halo, saya ${this.nama}, ${this.umur} tahun`);
//     }
// }

// object literal
const mhs1 = {
    nama: "Mahardika",
    umur: 18,
    sayHello: () => {
        console.log(`Halo, nama saya ${this.nama}, ${this.umur} tahun`);
    }
}

// arrow function tidak punya konsep this
const Mahasiswa = function() {
    this.nama = "Mahardika";
    this.umur = 18;
    this.sayHello = function() {
        console.log(`Halo, saya ${this.nama}, ${this.umur} tahun`);
    }
    
    // setInterval(() => {
    //     console.log(this.umur++);
    // }, 500);
}

const mahardika = new Mahasiswa();