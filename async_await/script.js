const test = new Promise(resolve => {
    setTimeout(() => {
        resolve('OK')
    }, 2000);
});

// synchronous sehingga menghasilkan pending
// console.log(test);

// asynchronous
// test.then(() => console.log(test));

function cobaPromise() {
     return new Promise((resolve, reject) => {
        const waktu = 5000;
        if(waktu < 5000) {
            setTimeout(() => {
                resolve('OKE');
            }, waktu);
        } else {
            reject('Kelamaan bang!');
        }
     });
}

const coba = cobaPromise();
// synchronous menghasilkan pending
// console.log(coba);

// asynchronous pakai then
// coba
//     .then(() => console.log(coba))
//     .catch(() => console.log(coba));


// menggunakan async dan await agar terlihat seperti synchronous
// cara ini hanya bisa menangkap resolve
// async function cobaAsync() {
//     const coba = await cobaPromise();
//     console.log(coba);
// }

// cara menangkap resolve dan reject
async function cobaAsync() {
    try {
        const coba = await cobaPromise();
        console.log(coba);
    } catch(e) {
        console.log(e);
    }
}

cobaAsync();