// 1. object literal
let mahasiswa = {
    nama: "Mahardika",
    energi: 10,
    makan: function(porsi) {
        this.energi += porsi;
        console.log(`Halo ${this.nama}, selamat makan!`);
    }
};

// 2. function declaration
function mhs(nama, energi) {
    let mahasiswa = {};
    mahasiswa.nama = nama;
    mahasiswa.energi = energi;

    mahasiswa.makan = function(porsi) {
        this.energi += porsi;
        console.log(`Halo ${this.nama}, selamat menambah energi!`);
    };

    mahasiswa.main = function(jam) {
        this.energi -= jam;
        console.log(`Halo ${this.nama}, selamat bermain!`);
    };

    return mahasiswa;
}
let mhs1 = mhs("Sandhika Galih", 10);
let mhs2 = mhs("Mahardika", 15);

// 3. constructor
function Mahasiswa(nama, energi) {
    this.nama = nama;
    this.energi = energi;

    this.makan = function(porsi) {
        this.energi += porsi;
        console.log(`Halo ${this.nama}, selamat makan!`);
    }

    this.main = function(jam) {
        this.energi -= jam;
        console.log(`Halo ${this.nama}, selamat bermain!`);
    }
}
let maha = new Mahasiswa("Mahardika", 20);