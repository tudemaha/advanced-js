/*
1. Object literal
Problem: tidak efektif untuk membuat banyak object

2. Function declaration
Problem: di belakang layar, setiap instansiasi dibuat ulang methodnya
    bisa diakali dengan membuat object terpisah di luar object utama
    Namun, saat membuat method baru di object di luar, harus didaftarkan di object utama. bisa diakali dengan menggunaan Object.create()
*/

// Function declaration
const methodMahasiswa = {
    makan: function(porsi) {
        this.energi += porsi;
        console.log(`Halo ${this.nama}, selamat makan!`);
    },

    main: function(jam) {
        this.energi -= jam;
        console.log(`Halo ${this.nama}, selamat bermain!`);
    },

    tidur: function(jam) {
        this.energi += 2 * jam;
        console.log(`Halo ${this.nama}, selamat tidur!`);
    }

};

function Mahasiswa(nama, energi) {
    // gunakan Object.create() agar tidak usah menghubungkan
    let mahasiswa = Object.create(methodMahasiswa);
    mahasiswa.nama = nama;
    mahasiswa.energi = energi;

    // tidak usah diisi jika menggunakan Object.create()
    // mahasiswa.makan = methodMahasiswa.makan;
    // mahasiswa.main = methodMahasiswa.main;
    // mahasiswa.tidur = methodMahasiswa.tidur;

    return mahasiswa;
}

let mahardika = Mahasiswa("Mahardika", 10);
let anto = Mahasiswa("Anto", 12);