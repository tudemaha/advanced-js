// prototypal inheritance
function Mahasiswa(nama, energi) {
    // yang terjadi di belakang layar
    // let this = Object.create(Mahasiswa.prototype);

    this.nama = nama;
    this.energi = energi;

    // return this;
}

Mahasiswa.prototype.makan = function(porsi) {
    this.energi += porsi;
    return `Halo ${this.nama}, selamat makan!`;
}

Mahasiswa.prototype.main = function(jam) {
    this.energi -= jam;
    return `Halo ${this.nama}, selamat bermain!`;
}

Mahasiswa.prototype.tidur = function(jam) {
    this.energi += 2 * jam;
    return `Halo ${this.nama}, selamat tidur!`;
}

let mahardika = new Mahasiswa("Mahardika", 10);


// class version (di belakang layar yang dijalankan prototype)
class Mhs {
    constructor(nama, energi) {
        this.nama = nama;
        this.energi = energi;
    }

    makan(porsi) {
        this.energi += porsi;
        return `Halo ${this.nama}, selamat makan!`;
    }

    main(jam) {
        this.energi -= jam;
        return `Halo ${this.nama}, selamat bermain!`;
    }

    tidur(jam) {
        this.energi += 2 * jam;
        return `Halo ${this.nama}, selamat tidur!`;
    }
}
let tude = new Mhs("Tude", 10);

/*
prototype ini perlu dipelajari karena object sangat berperan besar dalam javascript
misal saat membuat array, pasti ada prototype untuk Array

sehingga array yang dibuat mewarisi semua method dari object array

let angka = [];
// yang terjadi di belakang layar
function Array() {
    let this = Object.create(Array.prototype);
}
*/
