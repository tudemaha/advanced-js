var nama = "Mahardika";
console.log(nama);

/*
1. creation phase pada global context
    - mengecek apakah ada variabel atau function di kodingan
    - jika ada, variabel akan dibuat variabel dan diisi undefined
                function akan diisi dengan kode function
        var nama = undefined
        function nama = fn()
    - disebut dengan hoisting
    - mendefinisikan object window sebagai global object
    - this = window

2. execution phase
    program dijalankan baris per baris

    makanya saat ada console.log(nama) jadi undefined, karena belum diisi nilai nama (belum sampai ke baris inisialisasi)

*/


var nama = "Tude";
var umur = 18;

console.log(sayHello());

function sayHello() {
    // console.log(`Halo, nama saya ${nama}, saya ${umur} tahun`);
    return `Halo, nama saya ${nama}, saya ${umur} tahun`; // agar tiak isi undefined
}

/*
Functio membuat local execution context
yang di dalamnya terdapat creation dan execution context
punya akses:
    window
    arguments
hoisting local
*/

var nama = "Mahardika";
var username = "tudemaha";

function cetakURL() {
    console.log(arguments);
    var instagram = "https://instagram.com/";
    return instagram + username;
}

console.log(cetakURL("mahardikaap"));

function a() {
    console.log("ini a");
    function b() {
        console.log("ini b");
        function c() {
            console.log("ini c");
        }
        c();
    }
    b();
}
a();