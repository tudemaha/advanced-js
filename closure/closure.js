function init() {
    let nama = "Mahardika"; // local variable
    return function(nama1) { // inner function (closure => mengunakan variabel dari luar fungsi (fungsi parent))
        console.log(nama1);
    }
    // tampilNama();
    // console.dir(tampilNama);
    // return tampilNama;
}

let panggilNama = init();

// panggilNama("Mahardika");
// panggilNama("anto");

/*
Mengapa menggunakan closure?
    membuat function factories
    membuat private method
*/

function ucapkanSalam(waktu) {
    return function(nama) {
        console.log(`Halo ${nama}. Selamat ${waktu}, semoga harimu menyenangkan!`);
    }
}

let selamatPagi = ucapkanSalam("pagi");
let selamatSiang = ucapkanSalam("siang");
let selamatSore = ucapkanSalam("sore");

// selamatPagi("Mahardika");
// selamatSore("Anto");
// console.dir(selamatPagi);

// membuat seolah-olah ada private function
let add = function() {
    let counter = 0;
    return function() {
        console.log(++counter);
    }
}

let a = add();

counter = 100;

a();
a();
a();

// dengan cara ini, var counter di dalam fungsi tidak akan terganggu oleh counter di luar