// rest parameter menggunakan ...namaVar
const nama = 'Mahardika';
const umur = 18;
const alamat = "Bali";

function test(strings, ...values) {
//     let result = '';
//     strings.forEach((str, index) => {
//         result += `${str}${values[index] || ''}`;
//     });
//     return result;

    // menggunakan reduce 
    return strings.reduce((result, str, i) => `${result}${str}${values[i] || ''}`, '');
}

const str = test`Halo, nama saya ${nama}, umur ${umur} tahun.`;
console.log(str);

// Highlighting
function highlight(strings, ...values) {
    return strings.reduce((result, str, i) => `${result}${str}<span class="hl">${values[i] || ''}</span>`, '');
}

const penting = highlight`Halo, nama saya ${nama}, berumur ${umur} tahun, berasal dari ${alamat}.`;
console.log(penting);
document.body.innerHTML = penting;

// fungsi lain tagged template
// - escaping HTML tags
// - translation & internationalization
// - styled components