// HTML Fragments
const mhs = {
    nama: "Mahardika",
    umur: 18,
    alamat: "Bali",
    email: "akukamu@cinta.com"
};

const el = `<div class="mhs">
    <h2>${mhs.nama}</h2>
    <span class="umur">${mhs.umur}</span>
</div>`;
// document.body.innerHTML = el;

// menggunakan looping
const students = [
    {
        nama: "Mahardika",
        email: "akukamu@cinta.com"
    },
    {
        nama: "Tude",
        email: "maha@cinta.com"
    },
    {
        nama: "Yanto",
        email: "yanto@cinta.com"
    }
];

const elements = `<div class="mhs">
    ${students.map(student => `<ul>
        <li>${student.nama}</li>
        <li>${student.email}</li>
    </ul>`).join('')}
</div>`
// document.body.innerHTML = elements;

// kondisional
const lagu = {
    judul: "Oh My God",
    penyanyi: "Nosstress",
    // feat: "Rara Sekar"
}

const tampilanLagu = `<div class="lagu">
    <li>Judul: ${lagu.judul}</li>
    <li>Penyanyi: ${lagu.penyanyi} ${lagu.feat ? `(feat. ${lagu.feat})` : ``}</li>
</div>`
// document.body.innerHTML = tampilanLagu;

// nested (HTML fragments bersarang)
const college = {
    nama: "Budi",
    semester: 1,
    mataKuliah: ["Algoritma", "Sistem Digital", "Basis Data", "Matematika Diskrit", "Aljabar Linear Elementer"]
};

// menggunakan function biasa
// function cetakMataKuliah(mataKuliah) {
//     return `<ol>
//         ${mataKuliah.map(mk => `<li>${mk}</li>`).join('')}
//     </ol>`
// }

const html = `<div class="college">
    <h2>${college.nama}</h2>
    <span class="semester">Semester: ${college.semester}</span>
    <h4>Mata Kuliah:</h4>
    <ol>
        ${college.mataKuliah.map(mk => `<li>${mk}</li>`).join('')}
    </ol>
</div>`
document.body.innerHTML = html;