/*
kegunaan template literals
- multiline string
- embedded expression
- html framgments
- expression interpolation
- tagged template
*/

// multiline string
console.log(`string 1
string 2`);

// html fragments
const mhs = {
    nama: 'Mahardika',
    umur: '19',
    alamat: 'Bali'
};

let el = `<div>
    <h2>${mhs.nama}</h2>
    <span class"alamat">${mhs.alamat}</span>
</div>`;
console.log(el);


// embedded expression
const nama = 'Mahardika';
let umur = 18;
console.log(`Halo, perkenalkan saya ${nama}, umur ${umur} tahun.`);

// expression interpolation
let a = 1, b = 5;
console.log('Jika a = 1 dan b = 5, maka hasil penjumlahannya adalah ' + (a + b) + ' bukan ' + (2 * a + b));
console.log(`Jika a = 1 dan b = 5, maka hasil penjumlahannya adalah ${a + b}, bukan ${2 * a + b}`);