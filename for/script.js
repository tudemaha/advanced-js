/*
for of => iterable
for in => enumerable
*/

// untuk array
const mhs = ["Mahardika", "Made", "Kade"];
// for(let i = 0; i < mhs.length; i++) {
//     console.log(mhs[i]);
// }
// console.log("\n");

// mhs.forEach(m => console.log(m));
// mhs.forEach((m, i) => {
    //     console.log(`${m} adalah mahasiswa ke-${i + 1}`)
    // });
// console.log("\n");

// for(const m of mhs) console.log(m);
// for(const [i, m] of mhs.entries()) {
    //     console.log(`${m} adalah mahasiswa ke-${i}`);
    // };
    
// untuk string
const nama = "Mahardika";
// for(const n of nama) console.log(n);

// nodelist
const liNama = document.querySelectorAll('.nama');
// liNama.forEach(n => console.log(n.textContent));
// for(const li of liNama) console.log(li.innerHTML);

// arguments
function jumlahkan() {
    // arguments tidak bisa menggunakan reduce dan foreach
    let jumlah = 0;
    for(a of arguments) jumlah += a
    return jumlah;
}
console.log(jumlahkan(1, 2, 3, 4, 5, 6, 7, 8));
console.log("\n");

const orang = {
    nama: "Anto",
    umur: 14,
    alamat: "Bali"
};

for(p in orang) console.log(p);
for(p in orang ) console.log(orang[p]);