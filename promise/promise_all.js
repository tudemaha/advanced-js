// Promise.all() (untuk menjalankan semua promise sekaligus)
const film = new Promise(resolve => {
    setTimeout(() => {
        resolve([{
            judul: 'Single 2',
            actor: 'Raditya Dika'
        }]);
    }, 2000);
});

const weather = new Promise(resolve => {
    setTimeout(() => {
        resolve([{
            kota: 'Denpasar',
            temp: '29',
            keterangan: 'terang'
        }]);
    }, 1000)
});

// jalanin berbeda
// film.then(response => console.log(response));
// weather.then(response => console.log(response));

// jalanin bareng
Promise.all([film, weather])
    .then(response => console.log(response));

// menggunakan destructuring
Promise.all([film, weather])
    .then(response => {
        const [film, weather] = response;
        console.log(film);
        console.log(weather);
    });