// jQuery
$.ajax({
    url: 'http://www.omdbapi.com/?apikey=8cc2820&s=brontosaurus',
    success: result => {
        const movies = result.Search;
        console.log(movies);
    }
});
console.log("\n");

// vanillaJs
const xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
    if(xhr.status === 200) {
        if(xhr.readyState === 4) {
            const movies = JSON.parse(xhr.response);
            console.log(movies.Search);
        }
    } else {
        console.log(xhr.responseText);
    }
}
xhr.open('get', 'http://www.omdbapi.com/?apikey=8cc2820&s=brontosaurus');
xhr.send();
console.log("\n");

// menggunakan fetch
fetch('http://www.omdbapi.com/?apikey=8cc2820&s=brontosaurus')
    .then(response => response.json())
    .then(response => console.log(response));

// promise
// object yang merepresentasikan keberhasilan / kegagalan sebuah event yang asynchronous di masa yang akan datang
// janji (terpenuhi / ingkar)
// states (fulfilled / rejected / pending)
// callback (resolve / reject / finally(saat waktu tunggu selesai))
// aksi (then => menjalanka resolve / catch => menjalankan reject)

// contoh 1
let ditepati = true;
const janji1 = new Promise((resolve, reject) => {
    if(ditepati) {
        resolve('Janji ditepati');
    } else {
        reject('Ingkar janji');
    }
});

// waktu tunggu tidak terlihat
janji1
    .then(response => console.log("OK " + response))
    .catch(response => console.log("NOT OK " + response));


// contoh 2
let ditepati2 = false;
const janji2 = new Promise((resolve, reject) => {
    if(ditepati2) {
        setTimeout(() => {
            resolve('Ditepati setelah menggungu..');
        }, 2000);
    } else {
        setTimeout(()=> {
            reject('Tidak ditepati setelah beberapa waktu.');
        }, 4000);
    }
});

console.log('mulai');
// console.log(janji2.then(() => console.log(janji2)));  // terlihat ada pending
janji2
    .finally(() => console.log('Selsai menunggu'))
    .then(response => console.log(response))
    .catch(response => console.log(response));
console.log('selesai');

