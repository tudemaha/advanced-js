// memecah iterables menjadi single element
const mhs = ['Mahardika', 'Adi', 'Kade'];
const dosen = ['Widi', 'Supri', 'Lord'];
console.log(...mhs);


// menggabungkan dua array
const gabung =[...mhs,'Wkwk', ...dosen];    // spread operator bisa menggabungkan lebih fleksibel
// const gabung = mhs.concat(dosen);        // concat tidak sefleksibel spread operator
console.log(gabung);


// menduplikat array
const mhs_copy = [...mhs];
// const mhs_copy = mhs;   // cara ini membuat array duplikat terhubung ke array awal
mhs_copy[0] = 'Ari';       // sehingga saat mengubah array duplikat, array awal ikut berubah
console.log(mhs_copy);


// mengubah node list ke array
const liMhs = document.querySelectorAll('li');
console.log(liMhs);

// const mhsArr = [];
// for(let i = 0; i < liMhs.length; i++) {
//     mhsArr.push(liMhs[i].textContent);
// }
const mhsArr = [...liMhs].map(m => m.textContent);
console.log(mhsArr);


// menganimasikan kata huruf per huruf
const head = document.querySelector('.nama');
const huruf = [...head.textContent].map(h => `<span>${h}</span>`).join('');
head.innerHTML = huruf;
console.log(huruf);