// Synchronous callback
function halo(nama) {
    alert(`Halo, ${nama}`);
}

function showMessage(callback) {
    const nama = prompt('Masukkan nama:');
    callback(nama);
}

// showMessage(halo);  // menggunakan fungsi lain
// showMessage(nama => alert(`Hai halo, ${nama}`));    // langsung membuat fungsi

const mhs = [
    {
        nama: 'Mahardika',
        nim: '2107654388',
        kelas: 'B'
    },
    {
        nama: 'Yusuf',
        nim: '2109874633',
        kelas: 'A'
    },
    {
        nama: 'Budi',
        nim: '21876407855',
        kelas: 'C'
    }
];

// console.log('mulai');
// mhs.forEach(m => {
//     for(let i = 0; i < 10000000; i++) {
//         let date = new Date();
//     }
//     console.log(m.nama);
// });
// console.log('selesai');


// Asynchronous callback
// vanilla.js ajax
// function getDataMahasiswa(url, success, error) {
//     let xhr = new XMLHttpRequest();

//     xhr.onreadystatechange = function() {
//         if(xhr.readyState === 4) {
//             if(xhr.status === 200) {
//                 success(xhr.response);
//             } else if(xhr.status === 404) {
//                 error();
//             }
//         }
//     }

//     xhr.open('get', url);
//     xhr.send();
// }

// console.log('mulai');
// getDataMahasiswa('data/mahasiswa.json', result => {
//     let mhs = JSON.parse(result);
//     mhs.forEach(m => console.log(m.nama));
// }, () => {})
// console.log('selesai');

// jQuery
console.log('mulai');
$.ajax({
    url: 'data/mahasiswa.json',
    success: (mhs) => {
        mhs.forEach(m => console.log(m.nama));
    },
    error: (e) => {
        console.log(`Terjadi error ${e.responseText}`)
    }

});
console.log('selesai');