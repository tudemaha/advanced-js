// rest parameter => merepresentasikan argument pada function dengan jumlah yang tidak terbatas menjadi sebuah array

function myFunc(...myArgs) {
    // return arguments;
    // dalam bentuk array
    // return myArgs;
    return Array.from(arguments);
    return [...arguments];
}
console.log(myFunc(1, 2, 3, 4, 5));

function jumlah(...angka) {
    // menggunakan for of
    // let jumlah = 0;
    // for(const a of angka) jumlah += a;
    // return jumlah;

    // menggunakan reduce
    return angka.reduce((a, i) => a + i);
}
console.log(jumlah(1, 2, 3, 4, 5));

// array destructuring
const kelompok1 = ['Mahardika', 'Erik', 'Budi', 'Yanto', 'Made'];
const [ketua, wakil, ...anggota] = kelompok1;
console.log(anggota);

// object destructuring
const team = {
    pm: 'Andi',
    fe: 'Madya',
    be: 'Mahardika',
    do: 'Yusuf'
}
const {pm, ...member} = team;
console.log(member);

// filtering
function filtering(type, ...values) {
    return values.filter(v => typeof v === type);
}
console.log(filtering('boolean', 1, 2, '3', 'Mahardika', '7', true, 9.0, false));