// ambil elemen semua elemen video
const videos = Array.from(document.querySelectorAll('[data-duration]'));
console.log(videos);

// pilih hanya javascript lanjutan
let jsVideo = videos.filter(video => video.textContent.includes('JAVASCRIPT LANJUTAN'))

// ambil durasi masing-masing video (strint to int, menit to detik)
    .map(item => item.dataset.duration)
    .map(waktu => {
        const parts = waktu.split(':')
            .map(part => parseInt(part));
        return parts[0] * 60 + parts[1];
    })

// jumlahkan semua detik
    .reduce((total, detik) => total + detik);

// jumlahkan format ke (jam:menit:detik)
const jam = Math.floor(jsVideo / 3600);
jsVideo -= jam * 3600;

const menit = Math.floor(jsVideo / 60);

const detik = jsVideo - menit * 60;

// simpan di dom
const durasi = document.querySelector('.total-durasi');
durasi.textContent = `${jam} jam ${menit} menit ${detik} detik`;

// jumlah video
const jumlah = document.querySelector('.jumlah-video');
const jumlahVideo = videos.filter(video => video.textContent.includes('JAVASCRIPT LANJUTAN')).length;
jumlah.textContent = `${jumlahVideo} video`;
