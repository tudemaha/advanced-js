// callback => function yang menjadi argument 
// function yang mempunyai argument callback adalah higher order function

function repeat(n, action) {
    for(let i = 0; i < n; i++) {
        action(i);
    }
}

repeat(8, alert);

// contoh higher order function