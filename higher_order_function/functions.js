const angka = [-1, 8, 1, -9, 0, 5, 3, 8, -6, 2];

// menggunakan filter (mencari angka >= 3)
// looping biasa
const arrBaru = [];
for(let i = 0; i < angka.length; i++) {
    if(angka[i] >= 3) arrBaru.push(angka[i]);
}
console.log(arrBaru);

// function expression
let newAngka = angka.filter(function(a) {
    return a >= 3;
});
console.log(newAngka);

// array function
newAngka = angka.filter(a => a >= 3);
console.log(newAngka);

// map (kalikan semua angka dengan 2)
const kaliDua = angka.map(a => a * 2);
console.log(kaliDua);

// reduce (jumlahkan seluruh elemen pada array)
const jumlahAngka = angka.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
// terdapat parameter terakhir yaitu initial value
console.log(jumlahAngka);

// method chaining
// cari angka > 5, kalikan 3, jumlahkan semua
const hasil = angka.filter(a => a > 5)
    .map(a => a * 3)
    .reduce((acc, curr) => acc + curr);
console.log(hasil);