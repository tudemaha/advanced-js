// destructuring return value
function kalkulasiArray(a, b) {
    return [a + b, a - b, a * b, a / b];
}

// const jumlah = kalkulasiArray(2, 3)[0];
// const kali = kalkulasiArray(2, 3)[1];
// const [tambah, kurang, kali, bagi, pangkat = 'tidak ada'] = kalkulasiArray(2, 3);

// jika me return array, maka yang destructuring harus pas
// agar tidak salah penempatan variabel
// cara menangani adalah ubah return value menjadi object
function kalkulasiObject(a, b) {
    return {
        tambah: a + b,
        kurang: a - b,
        kali: a * b,
        bagi: a / b
    }
}

const {tambah, kali, kurang, bagi} = kalkulasiObject(3, 4);
console.log(kurang);

// destructuring function argument (parameter)
const mhs1 = {
    nama: "Mahardika",
    umur: 18,
    alamat: "Bali",
    mataKuliah: {
        sistemDigital: {
            matkul: "Sistem Digital",
            tugas: 80,
            uts: 88
        },
        pemrograman: {
            matkul: "Pemrograman",
            tugas: 89,
            uts: 90
        }
    }
}

// pecah di parameter
function cetakMhsA(nama, umur) {
    return `Halo, nama saya ${nama}, umur ${umur} tahun.`;
}
console.log(cetakMhsA(mhs1.nama, mhs1.umur))

// pecah di function (fungsi menerima object)
function cetakMhsB(mhs) {
    return `Halo, nama saya ${mhs.nama}, umur ${mhs.umur} tahun.`;
}
console.log(cetakMhsB(mhs1));

// menggunakan destructuring
function cetakMhsC({nama, umur}) {
    return `Halo, nama saya ${nama}, umur ${umur} tahun. `;
}
console.log(cetakMhsC(mhs1));

// contoh
function dataMhs({nama, umur, mataKuliah: {sistemDigital: {matkul, uts}}}) {
    return `Halo, saya ${nama} berusia ${umur} tahun. Nilai UTS ${matkul} saya ${uts}.`;
}
console.log(dataMhs(mhs1));