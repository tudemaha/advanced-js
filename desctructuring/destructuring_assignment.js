// // [] untuk array
// const perkenalan = ['Halo', 'saya', 'Mahardika'];
// // const salam = perkenalan[1];
// // const nama = perkenalan[2];

// // destructing
// // const [salam, satu, nama] = perkenalan;

// // skipping
// const [salam,,nama] = perkenalan;
// console.log(nama);

// // swapping
// let a = 10, b = 5;
// console.log(a, b);
// [a, b] = [b, a];
// console.log(a, b);

// // return value
// function test() {
//     return [1, 2];
// }

// const coba = test();
// const [val1, val2] = test();
// console.log(val1);

// // rest parameter
// const [nilai1, ...nilai] = [1, 2, 3, 4];
// console.log(nilai);

// // {} untuk object
// const mhs = {
//     nama: 'Mahardika',
//     umur: 19
// };

// // nama properti harus sama
// const {nama, umur} = mhs;

// atau bisa seperti ini
// ({nama, umur} = {nama: 'Mahardika', umur: 18});

// assign ke variabel baru
// ({nama: n, umur: u} = {nama: 'Mahardika', umur: 18});

// memberikan default value
const mhs = {
    id: 123,
    nama: "Mahardika",
    umur: 18,
    email: 'gede@cinta.com'
};
// assign ke variabel baru
// const {nama: n, umur: u, email: e = 'kosong'} = mhs;

// rest parameter
// const {nama, ...values} = mhs;

// mengambil field pada object, setelah dikirim sebagai parameter untuk function
function getMhsId({id}) {return id}

console.log(getMhsId(mhs));;
