/*
Bahan:
1. OMDb: Open Movie Database
2. Postman atau Imsomnia
3. Bootstrap
4. jQuery (untuk ajax)
*/

// fetch
const searchButton = document.querySelector('.search-button');
searchButton.addEventListener('click', function() {

    const keyword = document.querySelector('.input-keyword');
    fetch("http://www.omdbapi.com/?apikey=8cc2820&s=" + keyword.value)
        .then(response => response.json())
        .then(response => {
            const movies = response.Search;
            let cards = '';
            movies.forEach(movie => cards += showMovie(movie));
            const movieContainer = document.querySelector('.movie-container');
            movieContainer.innerHTML = cards;

            // when clicked
            const modalDetailButton = document.querySelectorAll('.modal-detail');
            modalDetailButton.forEach(btn => {
                btn.addEventListener('click', function() {
                    const imdbid = this.dataset.imdbid;
                    fetch("http://www.omdbapi.com/?apikey=8cc2820&i=" + imdbid)
                        .then(response => response.json())
                        .then(details => {
                            const modal_list = showDetails(details);
                            const modalBody = document.querySelector('.modal-body');
                            modalBody.innerHTML = modal_list;
                        });
                });
            })
        });
}); 

function showMovie(movie) {
    return `<div class="col-sm-3 my-4">
                <div class="card">
                    <img src="${movie.Poster}" class="card-img-top img-thumb" alt="${movie.Title}">
                    <div class="card-body">
                        <h5 class="card-title">${movie.Title}</h5>
                        <h6 class="card-subtile mb-2 text-muted">${movie.Year}</h6>
                        <a href="#" class="btn btn-primary modal-detail" data-bs-toggle="modal" data-bs-target="#movieModal" data-imdbid="${movie.imdbID}">Details</a>
                    </div>
                </div>
            </div>`
}

function showDetails(details) {
    return `<div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="text-center"><img src="${details.Poster}" alt="${details.Title}" class="img-fluid"></div>
                    </div>
                    <div class="col-md">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><h4>${details.Title} (${details.Year})</h4></li>
                            <li class="list-group-item"><strong>Genre: </strong>${details.Genre}</li>
                            <li class="list-group-item"><strong>Director: </strong>${details.Director}</li>
                            <li class="list-group-item"><strong>Actor(s): </strong>${details.Actors}</li>
                            <li class="list-group-item"><strong>Writer(s): </strong>${details.Writer}</li>
                            <li class="list-group-item"><strong>Plot:</strong><br>${details.Plot}</li>
                        </ul>
                    </div>
                </div>
            </div>`
}